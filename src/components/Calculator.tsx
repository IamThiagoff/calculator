"use client";

import React, { useState } from 'react';
import { Button } from "@/components/ui/button"

const Calculator: React.FC = () => {
  const [input1, setInput1] = useState<string>('');
  const [input2, setInput2] = useState<string>('');
  const [result, setResult] = useState<string>('');

  const handleCalculate = () => {
    const num1 = parseInt(input1, 10);
    const num2 = parseInt(input2, 10);

    if (isNaN(num1) || isNaN(num2)) {
      setResult('Erro: Entrada inválida');
      return;
    }

    setResult((num1 % num2).toString());
  };

  const handleClear = () => {
    setInput1('');
    setInput2('');
    setResult('');
  };

  return (
    <div className="max-w-sm mx-auto p-4 bg-white rounded-lg shadow-lg">
      <div className="mb-4">
        <input
          type="text"
          value={input1}
          onChange={(e) => setInput1(e.target.value)}
          placeholder="Número 1"
          className="w-full p-2 border rounded mb-2"
        />
        <input
          type="text"
          value={input2}
          onChange={(e) => setInput2(e.target.value)}
          placeholder="Número 2"
          className="w-full p-2 border rounded"
        />
        <div className="mt-2 text-xl">{result}</div>
      </div>
      <div className="grid grid-cols-2 gap-2">
        <Button onClick={handleCalculate}>Calcular MOD</Button>
        <Button onClick={handleClear}>Limpar</Button>
      </div>
    </div>
  );
};

export default Calculator;
