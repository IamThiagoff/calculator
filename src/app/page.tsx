import React from 'react';
import Calculator from '../components/Calculator';

const Home: React.FC = () => {
  return (
    <div className="h-screen flex items-center justify-center bg-gray-100">
      <Calculator />
    </div>
  );
};

export default Home;
